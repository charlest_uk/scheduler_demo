scheduler_demo
C++ old school coroutine based scheduling example

## Description
This is based on how I used to do co-operative multi-tasking scheduling for
bespoke video effects hardware based on the Motorola 68000.
I used to call it pre-emptive scheduling in those days because it would init
the hardware and all the tasks would go quiescent until the user of the system
hit a control panel button, i.e. giving it a pre-emptive input.

## Installation
I have only built this on an intel based Macbook Pro running Monterey macOS 12.1 using clang 13.0.0
with the following command line:
        clang -std=c++20 -lc++ CoTask.cpp

## Roadmap
Will just keep doing mods to simplify and make suitable as a simple multi-tasking scheduler demo.

## Contributing
Any input gratefully received.

## Authors and acknowledgment
Based on Nico Josuttis demo code from ACCU 2022 two day workshop on C++20. Thanks Nico!

## License
For open source projects, say how it is licensed.
